<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Pencarian
            <small>Pilihan Belanja Anda - berdasarkan pencarian</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="index.html">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="#">Pencarian</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Hasil</span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<div class="m-heading-1 border-green m-bordered">
    <p>
        <form class="search-form" action="<?php echo base_url();?>welcome/generate" method="POST">
            <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon input-circle-left">
                    <i class="fa fa-search"></i>
                </span>
                <input type="text" name="shooping" class="form-control input-circle-right" placeholder="Pencarian Produk"> </div>
        </div>
        </form>
    </p>
</div>
<!-- BEGIN PAGE BASE CONTENT -->
<div class="portfolio-content portfolio-1">
    <div id="js-filters-juicy-projects" class="cbp-l-filters-button">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase"> All
            <div class="cbp-filter-counter"></div>
        </div>

        <?php foreach($toko as $shop){?>
        <div data-filter=".<?php echo $shop; ?>" class="cbp-filter-item btn dark btn-outline uppercase"> <?php echo $shop; ?>
            <div class="cbp-filter-counter"></div>
        </div>
        <?php }?>
    </div>

    <div id="js-grid-juicy-projects" class="cbp">
        <?php $jml_bli = count($blibli['blibli']['judul'])-1; ?>
        <?php for($x=0; $x<=$jml_bli; $x++){?>
        
        <div class="cbp-item blibli">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img src="<?php echo $blibli['blibli']['gambar'][$x]; ?>" alt=""> </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo $blibli['blibli']['link'][$x]; ?>" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow" target="_blank">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title uppercase text-center "><?php echo $blibli['blibli']['judul'][$x]; ?></div>
            <div class="cbp-l-grid-projects-title text-center " style="color: purple"><?php echo $blibli['blibli']['harga'][$x]; ?></div>
            <div class="cbp-l-grid-projects-desc uppercase text-center ">Toko Online Blibli.com</div>
        </div>
        <?php }?>

        <?php $jml_blk = count($bukalapak['bukalapak']['judul'])-1; ?>
        <?php for($i=0; $i<=$jml_blk; $i++){?>
        <div class="cbp-item bukalapak">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img src="<?php echo $bukalapak['bukalapak']['gambar'][$i]; ?>" alt=""> </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo "https://www.bukalapak.com".$bukalapak['bukalapak']['link'][$i]; ?>" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow" target="_blank">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title uppercase text-center"><?php echo $bukalapak['bukalapak']['judul'][$i]; ?></div>
            <div class="cbp-l-grid-projects-title text-center" style="color: purple">Rp <?php echo $bukalapak['bukalapak']['harga'][$i]; ?></div>
            <div class="cbp-l-grid-projects-desc uppercase text-center">Toko Online Bukalapak.com</div>
        </div>
        <?php }?>
    </div>
    <div id="js-loadMore-juicy-projects" class="cbp-l-loadMore-button">
        <a href="" class="cbp-l-loadMore-link btn grey-mint btn-outline" rel="nofollow">
            <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
            <span class="cbp-l-loadMore-loadingText">LOADING...</span>
            <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
        </a>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
