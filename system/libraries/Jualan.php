<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Jualan{
	
	function CookiesToStr($cookie=array()) {
		$cookies = "";
		foreach ($cookie as $k => $v) {
			$cookies .= "$k=$v;";
		}
		// Remove the last ';'
		//$cookies = substr($cookies, 0, -1);
		return $cookies;
	}

	function GetCookies($content) {
		// The U option will make sure that it matches the first character
		// So that it won't grab other information about cookie such as expire, domain and etc
		preg_match_all('/Set-Cookie: (.*)(;|\r\n)/U', $content, $temp);
		$cookie = $temp[1];
		$cook = implode('; ', $cookie);
		return $cook;
	}

	function GetCookiesArr($content, $del=true, $dval='deleted') {
		preg_match_all('/Set-Cookie: (.*)(;|\r\n)/U', $content, $temp);
		$cookie = array();
		foreach ($temp[1] as $v) {
			$v = explode('=', $v, 2);
			$cookie[$v[0]] = $v[1];
			if ($del && $v[1] == $dval) unset($cookie[$v[0]]);
		}
		return $cookie;
	}

	function GetCookiesAll($page, $cookie=0) {
		$cookiearr = GetCookiesArr($page);
		if($cookie)
			$cookiebaru = array_merge($cookie, $cookiearr);
		else
			$cookiebaru = $cookiearr;
		return $cookiebaru;
	}

	function cut_str($str, $left, $right) {
		$str = substr ( stristr ( $str, $left ), strlen ( $left ) );
		$leftLen = strlen ( stristr ( $str, $right ) );
		$leftLen = $leftLen ? - ($leftLen) : strlen ( $str );
		$str = substr ( $str, 0, $leftLen );
		return $str;
	}

	
	public function debug($page, $varay=false) {
		if (is_array($page)) $page = htmlspecialchars(print_r($page, true), ENT_QUOTES, 'UTF-8');
		echo "<br /><textarea cols='120' rows='30'>" . $page . "</textarea><br />";
		if($varay) print_r($varay);
	}
	
	function curl($url, $cookies=0, $post=0, $referrer=0, $XMLRequest=0, $proxyport=0) {	
		$url = html_entity_decode($url);
		
		$ch = @curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		if ($cookies) {
			if (is_array($cookies)) {
				$cookies = CookiesToStr($cookies);
			}
			curl_setopt($ch, CURLOPT_COOKIE, $cookies);	
		}
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0');
		if ($XMLRequest) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest"));
		}
		if($proxyport) {
			curl_setopt($ch, CURLOPT_PROXY, $proxyport);
		}
		curl_setopt($ch, CURLOPT_REFERER, $referrer); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($post){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 60);
		$page = curl_exec( $ch);
		curl_close($ch);
		
		return $page;
	}

	function readcookie($cookiepath) {
		$filename = $cookiepath;
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		if(is_serialized($contents)) {
			$contents = unserialize($contents);
		}
		fclose($handle);
		return $contents;
	}

	function writecookie($cookiepath, $cookievalue) {
		$myFile = $cookiepath;
		$fh = fopen($myFile, 'w') or die("Can't open specified cookie path");
		if (is_array($cookievalue)) {
			$cookievalue = serialize($cookievalue);
		}
		fwrite($fh, $cookievalue);
		fclose($fh);
	}

	public function enterrecaptcha($linkrecaptcha=0, $imgfilename, $variablenya, $post=0, $pesan=0) {
		global $nn, $download_dir;		
		
		if($linkrecaptcha) {
			$cook = 'PREF=ID=1111111111111111:FF=0:LD=en:TM=1435567280:LM=1435568580:GM=1:V=1:S=eGrokYKawxBOjBFN; NID=68=alxsX-AnNP-swf98ugdTzcpOglfvh3H0CRU3HZ4-I0ojUICPgdh0Dyzz_u_vI1JprvdO3GdssmdPD_1Qut2mHhc3_eiVptDPcND5Vu9GP9cJOEMuPIDqKe7CsVDk4TOlXGRNW1ZRTZ-M4D30dkswTE-OHzBU_aV4; OGP=-5061451:; OGPC=5061617-3:; SID=DQAAAC4BAAA0fjIjFv5IOsxCPG0XTiZqHOjnLud7SbYcS0TIoQXhF6bpckNaNOqNAnX9R3T0yPNH02GnxhwdGqL0xuRiyiCBxuQKuBmapInNcGZX1XG4XyCm6LI5TrcIlPi6wGAa_5SV106PSTeRVMrzLM52obdliZmn_4-wdKWFtXN9OnJtVeRKDNIazrxvyHS6D_Z2kPhT1O4fkgsOYAi9XJ-DcZ7B_UeoW2jJ_1qU1iHsYaN8RvuZ-FOqqJPAc0GrDJ9YEQDcdE_YPS9R0XJcoqN5eB0bkhSBLz3QbITcMWkiS5xU2SDaz2wRjWEXucED4OCH_f0sWwuUNmB8jGroG499YzcsFjE9niaaZJ8aQRp6WvWtvg7wGvJURAZm0bbwRGo7OLOwddrUmp1Z2INugkxFHz32; HSID=A_p0BKHH_s6LAz1yv; APISID=53QNHyVUd69xwkel/APHpMtb7gXmPOEPcy
';
			$page = $this->GetPage($linkrecaptcha, $cook);
			is_present($page, "Expired session", "Recaptcha ERROR - Expired session");
			$ch = cut_str($page, "challenge : '", "'");
			$page = $this->GetPage("http://www.google.com/recaptcha/api/image?c=".$ch);
			$headerend = strpos($page,"\r\n\r\n");
			$pass_img = substr($page,$headerend+4);
			$imgfile = $download_dir.$imgfilename;
			$fimg = fopen($imgfile,"w");
			fwrite($fimg,$pass_img);
			fclose($fimg);

			$recaptcha_challenge_field = $ch;
		}
		else {
			$imgfile = $imgfilename;
		}
		
		
		$code = '<center>';
		if($pesan) {
			$code .= '<BR/>'.$pesan.'<BR/>'.$nn.$nn;
		}
		$code .= '<BR/><h3 style=\'text-align: center;\'>Silakan masukkan captcha di bawah ini dengan benar :<BR/><BR/>Klo gagal brati anda salah masukkan captcha, silakan leech ulang.<BR/><BR/>'.$nn.$nn;
		
		$code .= '<BR/><img src='.$imgfile.'>'.$nn;
		
		$code .= '<form name="dl" method="post" action="'.$PHP_SELF.(isset($_GET["idx"]) ? "?idx=".$_GET["idx"] : "").'">'.$nn;
		
		foreach ($variablenya as $name => $input) {
            $code .= '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $input . '" />'.$nn;
        }
		
		if($post) {
			$code .= '<input name="post" type="hidden" value="'.urlencode(serialize($post)).'" />'.$nn;
		}
		
		if($linkrecaptcha) {
			$code .= '<input type="hidden" name="recaptcha_challenge_field" value="'.urlencode($recaptcha_challenge_field).'">'.$nn;		
		}
		
		$code .= '<BR/><BR/><input size="30" type="text" name="captcha" value="" autofocus="autofocus" required="required" />'.$nn;
		
		$code .= '<input type="submit" name="action" value="Download" />'.$nn;
		
		$code .= '</form></center>';
		echo ($code) ;		
		
		/*
		// hapus captcha otomatis, kasih sleep biar ga kecepeten hapusnya
		sleep(10);
		if(file_exists($imgfile)) {
			unlink($imgfile);
		}
		*/
		exit();
	}
	
}
